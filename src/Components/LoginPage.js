import React , {useCallback} from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import { useDispatch } from 'react-redux';
import { addlogin } from '../Store/authSlice';
import Alert from '@mui/material/Alert';
import {useNavigate } from "react-router-dom";


export default function ComposedTextField() {

    const dispatch = useDispatch();
    const [logintext, setLogin] = React.useState('');
    const [passwordtext, setPswrd] = React.useState('');
    const [showalert, setAlert] = React.useState(false);

    const navigate = useNavigate();

    const toHomePage = useCallback((path) => {
        console.log(path)
       return  navigate(path, {replace: true}), [navigate]
      });
    const handleClickLogin= (event) => {
        if(logintext.length<3 || passwordtext.length<6){
            setAlert(true)
        }else{
            dispatch(addlogin({Login:logintext, Password:passwordtext}))
            setAlert(false)
            toHomePage('/')
        }
        
    };

    const handleChangeLogin = (event) => {
        setLogin(event.target.value);
    }

    const handleChangePassword = (event) => {
        setPswrd(event.target.value);
    }

    return (
        <>
            <Box
                component="form"
                sx={{
                    '& > :not(style)': { m: 1 },
                }}
                noValidate  
                autoComplete="off"
            >
                <TextField 
                id="outlined-basic" 
                label="Login" 
                variant="outlined" 
                value={logintext} 
                onChange={handleChangeLogin}
                error ={logintext.length > 3 ? false : true }
                helperText="min 3"/>
                <TextField 
                id="outlined-basic" 
                label="Password" 
                variant="outlined" 
                value={passwordtext} 
                onChange={handleChangePassword}
                error ={passwordtext.length > 6 ? false : true }
                helperText="min 6"
                />


            </Box>
            <Stack spacing={2} direction="row">

                <Button variant="contained">Cancel</Button>
                <Button variant="contained" onClick={handleClickLogin}>Login</Button>

            </Stack>
            {showalert? (<Alert severity="error">Введите корректные значения    </Alert>) : ""}
            
        </>

    );
}
