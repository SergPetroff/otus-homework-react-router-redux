import * as React from 'react';
import Alert from '@mui/material/Alert';
export default function ErrorPage(){
    return(
        <Alert severity="warning">Страница не найдена!</Alert>
    )
}