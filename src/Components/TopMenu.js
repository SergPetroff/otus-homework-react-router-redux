import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';

function ResponsiveAppBar() {


    return (
        <AppBar position="static">
            <Container maxWidth="xl">
                <Toolbar disableGutters>


                    <Typography variant="h6" color="inherit" component="div">
                        <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                            <Button sx={{ color: '#fff' }}>
                                Home
                            </Button>
                        </Box>

                    </Typography>
                    <Typography variant="h6" color="inherit" component="div">
                    <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                            <Button sx={{ color: '#fff' }}>
                                Login
                            </Button>
                        </Box>
                    </Typography>
                    <Typography variant="h6" color="inherit" component="div">
                    <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                            <Button sx={{ color: '#fff' }}>
                                Register
                            </Button>
                        </Box>
                    </Typography>
                    <Typography variant="h6" color="inherit" component="div">
                    <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                            <Button sx={{ color: '#fff' }}>
                                404
                            </Button>
                        </Box>
                    </Typography>

                </Toolbar>
            </Container>
        </AppBar>
    );
}
export default ResponsiveAppBar;
