import React, {useCallback} from 'react';
import { useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import {useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import {logout} from '../Store/authSlice';




export default function HomePage() {
    const navigate = useNavigate();
    const hendlerToLoginpage = useCallback((path) => {
        console.log(path)
       return  navigate(path, {replace: true}), [navigate]
      });
    
    const dispatch = useDispatch();

    const hendlerLogout = () => {
        dispatch(logout())
    }

    const currentuser = useSelector(state => state.authstate.auth)


    const Loggeduser = () => (<div className='homecontent'> 
    <span>Текущий пользователь:</span> <span>{currentuser.Login}</span>
    <br></br>
    <Button variant="contained" onClick={() => hendlerLogout('/login')}>Logout</Button>
    </div>)
    const Unloginuser = () => (<><h2>Вы не авторизированы.</h2>  <Button variant="contained" onClick={() => hendlerToLoginpage('/login')}>Login</Button></>)

    if(currentuser){
        return <Loggeduser/>
    }

    return <Unloginuser/>
}