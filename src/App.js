import  React , {useCallback} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import {Routes, Route, useNavigate } from "react-router-dom";
import HomePage from './Components/HomePage'
import ErrorPage from './Components/ErrorPage'
import LoginPage from './Components/LoginPage'
import RegisterPage from './Components/RegisterPage'
import CssBaseline from '@mui/material/CssBaseline'


function App() {
  const menuitems = [{
    name:"Home",
    path:'/'
  },{
    name:"Login",
    path:'/login'
  },{
    name:"Register",
    path:'/register'
  },{
    name:"404",
    path:'/error'
  }]
  const navigate = useNavigate();
  const handleOnClick = useCallback((path) => {
    
   return  navigate(path, {replace: true}), [navigate]
  });

 


  const itemsmenu = menuitems.map((item) => (<Typography key={item.path} variant="h6" color="inherit" component="div">
  <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
      <Button sx={{ color: '#fff' }} onClick={() => handleOnClick(item.path)}>
      {item.name}
      </Button>
  </Box>

</Typography>))
  return (
    <>
    <CssBaseline />
    <Container fixed>
    <AppBar position="static">
        <Container maxWidth="xl">
            <Toolbar disableGutters>
                {itemsmenu}
            </Toolbar>
        </Container>
    </AppBar>

    <Routes>
      <Route path="/" element={<HomePage/>}/>
      <Route path="/error" element={<ErrorPage/>}/>
      <Route path="/login" element={<LoginPage/>}/>
      <Route path="/Register" element={<RegisterPage/>}/>
    </Routes>
    </Container>
    </>
    
);
}

export default App;
