import { createSlice } from '@reduxjs/toolkit'

const authSlice = createSlice({
    name:"auth",
    initialState:{
        auth:null
    },
    reducers:{
        
        addlogin(state,action){
            
            state.auth = {
                Login:action.payload.Login,
                Password:action.payload.Password
            }
        },
        logout(state,action){

            state.auth = null
        },

    }
});

export const {addlogin, logout} = authSlice.actions;
export default authSlice.reducer;